cmake_minimum_required(VERSION 3.0)
project(kjsonserde LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt5 COMPONENTS Core Test)
find_package(ECM REQUIRED NO_MODULE)

set(CMAKE_MODULE_PATH 5.0 ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(KDECMakeSettings)

add_library(kjsonserde STATIC src/serdeobject.cpp src/serdeobject.h)
target_link_libraries(kjsonserde PUBLIC Qt::Core)
target_include_directories(kjsonserde PUBLIC src)

include(ECMAddTests)
ecm_add_test(tests/basic_test.cpp TEST_NAME basic_test LINK_LIBRARIES kjsonserde Qt::Test)
