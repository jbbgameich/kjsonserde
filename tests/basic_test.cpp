#include <serdeobject.h>

#include <QTest>
#include <QDebug>

struct StringToIntConverter {
    inline static int convert(const QJsonValue &val) {
        return val.toString().toInt();
    }
};

SERDE_STRUCT(TestClass2,
    Member<QVector<double>, "ids"> ids;
    Member<QStringList, "names"> names;
);

SERDE_STRUCT(TestClass,
     Member<int, "id", StringToIntConverter> id;
     Member<TestClass2, "friend"> f;
     Member<QString, "name"> name;
     Member<std::optional<int>, "year"> year;
);

class BasicTest : public QObject {
    Q_OBJECT

private:
    Q_SLOT void checkFields() {
        auto obj = SerdeObject::parse<TestClass>({
            {"id", "1234"},
            {"name", "User 1"},
            {"year", QJsonValue()},
            {"friend", {
                {
                     {"ids", QJsonArray {1,2}},
                     {"names", {}}
                }
            }}
        });
        QCOMPARE(obj.id, 1234);
        QCOMPARE(obj.name, "User 1");
        QVERIFY(!obj.year.value().has_value());
        qDebug() << obj.f->ids.value();
        QCOMPARE(*obj.f->ids, QVector({1.0, 2.0}));

        obj = SerdeObject::parse<TestClass>(R"(
{
    "id": "1234",
    "name": "User 1",
    "year": null,
    "friend": {
        "ids": [1, 2],
        "names": []
    }
}
)");
        QCOMPARE(obj.id, 1234);
        QCOMPARE(obj.name, "User 1");
        QVERIFY(!obj.year.value().has_value());

        auto obj2 = SerdeObject::parse<TestClass2>({
            {"ids", QJsonArray {1, 2, 3, 4}},
            {"names", QJsonArray {"User1", "User2"}}
        });
        QCOMPARE(obj2.names, QStringList({"User1", "User2"}));
        QCOMPARE(obj2.ids, QVector<double>({1, 2, 3, 4}));
    }
};

QTEST_GUILESS_MAIN(BasicTest);

#include "basic_test.moc"
