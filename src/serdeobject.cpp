#include "serdeobject.h"

#include <stack>

thread_local std::unordered_map<SerdeObject *, std::unordered_map<const char *, MemberBase *>> OBJECT_VTABLE;
thread_local std::stack<SerdeObject *> MOST_RECENTLY_CREATED_OBJECTS;

SerdeObject::SerdeObject() {
    registerSerdeObject(this);
}

SerdeObject::SerdeObject(SerdeObject &&other) noexcept {
    auto old = &other;
    auto n = this;

    OBJECT_VTABLE[n] = std::move(OBJECT_VTABLE[old]);
    unregisterSerdeObject(old);
}

SerdeObject::SerdeObject(SerdeObject &other) noexcept
{
    OBJECT_VTABLE[this] = OBJECT_VTABLE[&other];
}

SerdeObject::~SerdeObject() {
    unregisterSerdeObject(this);
}

void SerdeObject::loadFrom(const QJsonObject &from) {
    for (const auto &[name, member] : OBJECT_VTABLE[this]) {
        auto v = from.value(name);
        member->writeFunc(member, v);
    }
}

void registerSerdeObject(SerdeObject *obj) {
    OBJECT_VTABLE[obj] = {};
    MOST_RECENTLY_CREATED_OBJECTS.push(obj);
}

void unregisterSerdeObject(SerdeObject *obj) {
    OBJECT_VTABLE.erase(obj);
}

void registerMember(const char *name, MemberBase *member) {
    qDebug() << "Registering" << name << "in" <<  MOST_RECENTLY_CREATED_OBJECTS.top();
    auto &objVTable = OBJECT_VTABLE[MOST_RECENTLY_CREATED_OBJECTS.top()];
    objVTable[name] = member;
}

EndSerde::EndSerde()
{
    MOST_RECENTLY_CREATED_OBJECTS.pop();
}
