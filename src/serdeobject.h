#include <unordered_map>
#include <optional>
#include <algorithm>
#include <type_traits>

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

class SerdeObject;
struct MemberBase;

using PropertyWriterFunc = void(*)(MemberBase *, const QJsonValue &value);
struct MemberBase {
    PropertyWriterFunc writeFunc;
};

void registerSerdeObject(SerdeObject *obj);
void unregisterSerdeObject(SerdeObject *obj);
void registerMember(const char *name, MemberBase *member);

/**
 * Literal class type that wraps a constant expression string.
 *
 * Uses implicit conversion to allow templates to *seemingly* accept constant strings.
 */
template<size_t N>
struct StringLiteral {
    constexpr StringLiteral(const char (&str)[N]) {
        std::copy_n(str, N, value);
    }
    
    char value[N];
};

///
/// The SerdeObject class is the base class for all objects that should be directly deserializable from json.
///
/// It can be used as follows:
///
/// @code
///
/// SERDE_STRUCT(Person,
///     Member<QString, "name">                    name;
///     Member<int, "age">                         age;
///     Member<std::optional<QString>, "pronouns"> pronouns;
/// )
///
/// @endcode
///
/// Every SerdeObject derived class can be used to deserialize json, like this:
///
/// @code
///
/// obj = SerdeObject::parse<Person>(R"(
/// {
///    "name": "Linus Torvalds",
///    "age":  52,
///    "pronouns": null
/// }
/// )");
///
/// @endcode
///
/// @warning An important limitation is currently, that these objects must never be used between threads,
/// since this breaks the internal reflection of the object
///
class SerdeObject {
public:
    SerdeObject();
    SerdeObject(SerdeObject &&) noexcept;
    SerdeObject(SerdeObject &) noexcept;
    SerdeObject &operator=(SerdeObject &&) = default;
    SerdeObject &operator=(const SerdeObject &) = default;
    ~SerdeObject();

    ///
    /// deserialize a QJSonObject into a SerdeObject derived class
    ///
    template <typename T>
    static T parse(const QJsonObject &from) {
        T out;
        out.loadFrom(from);
        return out;
    }

    ///
    /// parse a byte array containing json encoded data into a SerdeObject derived class
    ///
    template <typename T>
    static T parse(const QByteArray &from) {
        auto document = QJsonDocument::fromJson(from);
        return SerdeObject::parse<T>(document.object());
    }
private:
    void loadFrom(const QJsonObject &from);
};

namespace std {
    template <>
    struct hash<SerdeObject> {
        inline std::size_t operator()(SerdeObject const& s) const noexcept
        {
            return std::hash<void *>{}((void*)(&s));
        }
    };
}

template<typename T>
inline T fromJsonValue(const QJsonValue &val);

template<>
inline int fromJsonValue<int>(const QJsonValue &val) {
    return val.toInt();
}
template<>
inline int64_t fromJsonValue<int64_t>(const QJsonValue &val) {
    if constexpr (sizeof(int) == sizeof(int64_t)) {
        return val.toInt();
    } else {
        return val.toDouble();
    }
}
template<>
inline QString fromJsonValue<QString>(const QJsonValue &val) {
    return val.toString();
}
template<>
inline double fromJsonValue<double>(const QJsonValue &val) {
    return val.toDouble();
}

#define DECLARE_VEC_FROM_JSON_VALUE(Type) \
template<> \
inline Type fromJsonValue<Type>(const QJsonValue &val) { \
    Type out; \
    const auto jsonArray = val.toArray(); \
    out.reserve(jsonArray.size()); \
    for (const auto elem : jsonArray) { \
        using ElementT = decltype(*Type().begin()); \
        using ElementType = std::decay_t<ElementT>; \
        out.push_back(fromJsonValue<ElementType>(elem)); \
    } \
    return out; \
}

#define DECLARE_OPTIONAL_FROM_JSON_VALUE(Type) \
template<> \
inline Type fromJsonValue<Type>(const QJsonValue &val) { \
    using T = std::decay_t<decltype(*Type())>; \
    if (val.isNull()) { \
        return {}; \
    } else { \
        return { fromJsonValue<T>(val) }; \
    } \
}

#define DECLARE_CONTAINER_JSON_VALUE_OVERLOADS(Type) \
DECLARE_VEC_FROM_JSON_VALUE(QVector<Type>) \
DECLARE_VEC_FROM_JSON_VALUE(QList<Type>) \
DECLARE_VEC_FROM_JSON_VALUE(std::vector<Type>) \
DECLARE_OPTIONAL_FROM_JSON_VALUE(std::optional<Type>)

DECLARE_VEC_FROM_JSON_VALUE(QStringList);
DECLARE_CONTAINER_JSON_VALUE_OVERLOADS(QString);
DECLARE_CONTAINER_JSON_VALUE_OVERLOADS(int);
DECLARE_CONTAINER_JSON_VALUE_OVERLOADS(int64_t);
DECLARE_CONTAINER_JSON_VALUE_OVERLOADS(double);

template <typename T>
struct GenericValueConverter {
    inline static T convert(const QJsonValue &val) {
        qDebug() << Q_FUNC_INFO;
        if constexpr (std::is_base_of_v<SerdeObject, T> || std::is_base_of_v<T, SerdeObject>) {
            qDebug() << "Converting by recursing on other json object";
            return SerdeObject::parse<T>(val.toObject());
        } else {
            return ::fromJsonValue<T>(val);
        }
    }
};

template <typename T, StringLiteral lit, typename Converter>
class Member;

template <typename T, StringLiteral lit, typename Converter = GenericValueConverter<T>>
class Member : public MemberBase {
public:
    Member() : MemberBase() {
        registerMember(lit.value, this);
        this->writeFunc = [](auto *member, const QJsonValue &v) -> void {
            auto val = Converter::convert(v);
            static_cast<Member<T, lit, Converter> *>(member)->m_value = val;
        };
    }

    Member &operator=(T &&v) {
        m_value = v;
    }
    Member &operator=(T &v) {
        m_value = v;
    }
    
    bool operator==(const T &v) const {
        return m_value == v;
    }

    explicit operator T() const {
        return m_value;
    }

    T &operator *() {
        return m_value;
    }

    T value() {
        return m_value;
    }

    T* operator->() {
        return &m_value;
    }

private:
    T m_value;
};

struct EndSerde {
    EndSerde();
};

#define SERDE_STRUCT(name, members...) \
    struct name : SerdeObject { \
        members \
    private: \
        EndSerde end; \
    }
